package cn.jsonp.mywebview.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtil {


	/**
	 * 根据jsonStr对应的jsonObject，如果异常返回null
	 * 
	 * @param jsonStr
	 * @param keys
	 * @return
	 */
	public static JSONObject parseJSONObject(String jsonStr) {

		try {
			if (jsonStr != null && jsonStr.trim().length()>0) {
				JSONObject json = new JSONObject(jsonStr);
				return json;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 根据jsonStr对应的jsonArray，如果异常返回null
	 * 
	 * @param jsonStr
	 * @param deepParse
	 *            是否需要进一步解析
	 * @return
	 */
	public static JSONArray parseJSONArray(String jsonStr, boolean deepParse) {

		try {
			if (jsonStr != null && jsonStr.trim().length()>0) {
				JSONArray jsonArr = new JSONArray(jsonStr);
				if (deepParse) {
					JSONArray objects = new JSONArray();
					String objStr = null;
					for (int i = 0, len = jsonArr.length(); i < len; i++) {
						objStr = jsonArr.getString(i);
						JSONObject object = new JSONObject(objStr);
						objects.put(object);
					}
					return objects;
				} else {
					return jsonArr;
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取成功的jsonobject
	 * @return
	 * @throws JSONException 
	 */
	public static JSONObject getSuccessJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("success", true);
		return json;
	}
	
}
