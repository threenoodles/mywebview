package cn.jsonp.mywebview.utils;

import java.io.FileDescriptor;
import java.util.List;

import android.R;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;

public class Util {

	/**
	 * 显示通知
	 * 
	 * @param context
	 * @param title
	 * @param body
	 * @param notifyId
	 * @param startIntent
	 */
	public static void showNotification(Context context, String title,
			String body, int notifyId, Intent startIntent) {
		// 这里是弹出notifacation的代码
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(
				R.drawable.btn_star_big_on, title, System.currentTimeMillis());

		// notification.defaults = Notification.DEFAULT_VIBRATE;// 震动
		// notification.defaults = Notification.DEFAULT_SOUND;// 声音
		notification.defaults = Notification.DEFAULT_ALL;// 声音和震动
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				startIntent, 0);
		notification.setLatestEventInfo(context, title, body, contentIntent);
		notificationManager.notify(notifyId, notification);
	}

	/**
	 * 判断service 是否在运行
	 * 
	 * @param mContext
	 * @param className
	 * @return
	 */
	public static boolean isServiceRunning(Context mContext, String className) {

		boolean isRunning = false;
		ActivityManager activityManager = (ActivityManager) mContext
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> serviceList = activityManager
				.getRunningServices(30);

		if (!(serviceList.size() > 0)) {
			return false;
		}

		for (int i = 0; i < serviceList.size(); i++) {
			if (serviceList.get(i).service.getClassName().equals(className)) {
				isRunning = true;
				break;
			}
		}
		return isRunning;
	}

	/**
	 * 播放铃声
	 * 
	 * @param ctx
	 * @param type
	 */
	public static void playRing(MediaPlayer player,Context ctx) {
		if (player.isPlaying() || player.isLooping()) {
			return;
		}
		try {
			final AudioManager audioManager = (AudioManager) ctx.getSystemService(Context.AUDIO_SERVICE);
			if (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) != 0) {
				player.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
				player.setLooping(false);
				player.prepare();
				player.start();
			}
		} catch (Exception ex) { 
			ex.printStackTrace();
		}
	}
	
	/**
	 * 振动
	 * 
	 * @param ctx
	 * @param type
	 */
	public static void PlayVibrator(Vibrator vibrator) {
		long[] pattern = { 200, 300, 200, 300, 200 }; // 停止 开启// 停止 开启
		vibrator.vibrate(pattern, -1); // 重复两次上面的pattern // 如果只想震动一次，index设为-1
	}
}
