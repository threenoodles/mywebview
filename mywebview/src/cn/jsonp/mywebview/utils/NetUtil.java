package cn.jsonp.mywebview.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * 封装用户对网卡操作的类
 * 
 * @author Administrator
 */
public class NetUtil {

	// 网络已经连接
	public static final int NETWORK_STATUS_CONNECTED = 1;
	// 网络没有连接
	public static final int NETWORK_STATUS_UNCONNECTED = 0;

	/**
	 * 检查用户网络连接是否打开
	 * 
	 * @param context
	 */
	public static int checkInternet(Context context) {

		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();
		if (null != networkInfo) {

			for (int i = 0; i < networkInfo.length; i++) {

				if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED) {

					return NETWORK_STATUS_CONNECTED;
				}
			}
		}

		return NETWORK_STATUS_UNCONNECTED;
	}
}
