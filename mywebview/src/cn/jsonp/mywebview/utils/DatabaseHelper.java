package cn.jsonp.mywebview.utils;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	// 本地数据库版本
	private static final int DATABASE_VERSION = 1;
	// 本地数据库名称
	public static final String DATABASE_NAME = "myjsonp";

	public DatabaseHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);

	}

	public DatabaseHelper(Context context, String name) {

		super(context, name, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		initDatabase(db);
	}

	public static void initDatabase(SQLiteDatabase db) {
		// 用户
		String sql_article_drop = "drop table if exists j_article_detail";
		String sql_article_create = "create table j_article_detail (id int not null,detail text not null)";
		db.execSQL(sql_article_drop);
		db.execSQL(sql_article_create);
		System.out.println("init database success !");
	}

	/**
	 * 返回用户上次记录的楼宇 id name
	 * 
	 * @param restaurantsActivity
	 * @return
	 */
	public static void update(Context context, String sql, Object[] args) {

		DatabaseHelper dbHelper = new DatabaseHelper(context, DATABASE_NAME);
		SQLiteDatabase db = null;
		try {
			db = dbHelper.getWritableDatabase();
			db.execSQL(sql, args);
		} catch (Exception e) {
			System.out.println("sql exception : " + e);
		} finally {

			if (null != db) {
				db.close();
			}
		}
	}

	/**
	 * 查询数据
	 * 
	 * @param context
	 * @param tableName
	 * @param args
	 * @return
	 */
	public static HashMap<String, String> query(Context context, String sql,
			String[] args, String[] cols) {
		HashMap<String, String> results = new HashMap<String, String>();
		DatabaseHelper dbHelper = new DatabaseHelper(context, DATABASE_NAME);
		SQLiteDatabase db = null;
		Cursor cursor = null;

		try {
			db = dbHelper.getReadableDatabase();
			cursor = db.rawQuery(sql, args);
			while (cursor.moveToNext()) {

				for (String col : cols) {
					String _value = java.net.URLDecoder.decode(
							cursor.getString(cursor.getColumnIndex(col)),
							"UTF-8");
					results.put(col, _value);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return results;
		} finally {

			if (null != cursor) {
				cursor.close();
			}
			if (null != db) {
				db.close();
			}
		}
		return results;
	}

	/**
	 * 查询数据
	 * 
	 * @param context
	 * @param tableName
	 * @param args
	 * @return
	 * @throws JSONException
	 */
	public static String queryForJSONObject(Context context, String sql,
			String[] args, String[] cols) throws JSONException {

		JSONObject result = queryForObject(context, sql, args, cols);
		return result.length() > 0 ? result.toString() : "";
	}

	/**
	 * 查询数据
	 * 
	 * @param context
	 * @param tableName
	 * @param args
	 * @return
	 */
	public static String queryForJSONArr(Context context, String tablename, String[] cols) {

		return queryForArr(context, tablename, cols).toString();
	}

	/**
	 * 查询数据
	 * 
	 * @param context
	 * @param tableName
	 * @param args
	 * @return
	 */
	public static JSONArray queryForArr(Context context, String tablename,String[] cols) {
		JSONArray results = new JSONArray();
		DatabaseHelper dbHelper = new DatabaseHelper(context, DATABASE_NAME);
		SQLiteDatabase db = null;
		Cursor cursor = null;

		try {
			db = dbHelper.getReadableDatabase();
			cursor = db.query(tablename, cols, null, null, null, null,null);
			while (cursor.moveToNext()) {
				JSONObject data = new JSONObject();
				for (String col : cols) {
					String _value = java.net.URLDecoder.decode(
							cursor.getString(cursor.getColumnIndex(col)),
							"UTF-8");
					data.put(col, _value);
				}
				results.put(data); 
			}
		} catch (Exception e) {
			e.printStackTrace();
			return results;
		} finally {

			if (null != cursor) {
				cursor.close();
			}
			if (null != db) {
				db.close();
			}
		}
		return results;
	}

	
	/**
	 * @param context
	 * @param sql
	 * @param args
	 * @param cols
	 * @return
	 */
	public static JSONObject queryForObject(Context context, String sql,
			String[] args, String[] cols) {
		JSONObject result = new JSONObject();
		DatabaseHelper dbHelper = new DatabaseHelper(context, DATABASE_NAME);
		SQLiteDatabase db = null;
		Cursor cursor = null;

		try {
			db = dbHelper.getReadableDatabase();
			cursor = db.rawQuery(sql, args);
			while (cursor.moveToNext()) {
				for (String col : cols) {
					String _value = java.net.URLDecoder.decode(cursor.getString(cursor.getColumnIndex(col)),"UTF-8");
					result.put(col, _value);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		} finally {

			if (null != cursor) {
				cursor.close();
			}
			if (null != db) {
				db.close();
			}
		}
		return result;
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}
