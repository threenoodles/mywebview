package cn.jsonp.mywebview.activity;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import cn.jsonp.mywebview.R;
import cn.jsonp.mywebview.service.ArticleService;
import cn.jsonp.mywebview.service.WebAppInterface;
import cn.jsonp.mywebview.utils.Util;

@SuppressLint("SetJavaScriptEnabled")
public class MainActivity extends Activity {
	private WebView webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		System.out.println("mainActivity onCreate===>");
		setContentView(R.layout.activity_main);
		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		// 设置使用够执行JS脚本
		webView.getSettings().setBuiltInZoomControls(false);// 设置使支持缩放
		
		// appcache
		webView.getSettings().setAppCacheMaxSize(1024 * 1024 * 1024);
		String appCachePath = getApplicationContext().getCacheDir().getAbsolutePath();
		webView.getSettings().setAppCachePath(appCachePath);
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setAppCacheEnabled(true);

		//设置JS监听
		webView.addJavascriptInterface(new WebAppInterface(this), "_android");
		
		Intent intentStart = this.getIntent();
		String aid = intentStart.getStringExtra("aid");
		// 启动的时候带上aid，就认为是加载文章
		if (aid != null && !"".equals(aid)) {
			webView.loadUrl(this.getResources().getString(R.string.url_article)
					+ aid);
		} else {
			webView.loadUrl(this.getResources().getString(
					R.string.url_article_list));
		}

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);// 使用当前WebView处理跳转
				return true;// true表示此事件在此处被处理，不需要再广播
			}

			@Override
			// 转向错误时的处理
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Toast.makeText(MainActivity.this, "Sorry，出错了！",
						Toast.LENGTH_SHORT).show();
			}
		});
		// 初始化Activity
		MyApplication appContext = (MyApplication) this.getApplication();
		appContext.distroyAllActivity();
		appContext.addActivity(this);

		// 初始化Service
		if (Util.isServiceRunning(this, ArticleService.class.getName())) {
			System.out.println("service 已经运行中");
		} else {
			// 启动Service
			appContext.startService();
		}
	}

	private static Boolean isQuit = false;

	Timer timer = new Timer(); 

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (isQuit == false) {
				isQuit = true;
				Toast.makeText(getBaseContext(), "再按一次退出PUSH",
						Toast.LENGTH_SHORT).show();
				TimerTask task = null;
				task = new TimerTask() {
					@Override
					public void run() {
						isQuit = false;
					}
				};
				timer.schedule(task, 2000);
			} else {
				finish();
				System.exit(0);
			}
		}
		return false;
	}

	@Override
	protected void onPause() {
		System.out.println("mainActivity onPause===>");
		super.onPause();
	}
}
