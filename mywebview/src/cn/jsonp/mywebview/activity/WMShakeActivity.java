package cn.jsonp.mywebview.activity;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import cn.jsonp.mywebview.R;
import cn.jsonp.mywebview.service.ShakeListener;
import cn.jsonp.mywebview.service.WMAppInterface;

@SuppressLint("SetJavaScriptEnabled")
public class WMShakeActivity extends Activity {
	private WebView webView = null;
	private static Boolean isQuit = false;
	// 监听器
	private Timer timer = new Timer();
	// 通过network获取location
	private String networkProvider = LocationManager.NETWORK_PROVIDER;
	// 通过gps获取location
	private String GpsProvider = LocationManager.GPS_PROVIDER;
	// 定位
	LocationManager lm = null;
	Location location = null;
	LocationListener locationListener = null;

	// 监听摇一摇
	public static ShakeListener shake = null;
	// 定义sensor管理器, 注册监听器用
	private SensorManager mSensorManager = null;
	// 播放音乐
	private MediaPlayer player = null;
	// 震动
	Vibrator vibrator = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		System.out.println("mainActivity onCreate===>");
		setContentView(R.layout.activity_main);
		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		// 设置使用够执行JS脚本
		webView.getSettings().setBuiltInZoomControls(false);// 设置使支持缩放
		// appcache
		webView.getSettings().setAppCacheMaxSize(1024 * 1024 * 1024);
		String appCachePath = getApplicationContext().getCacheDir()
				.getAbsolutePath();
		webView.getSettings().setAppCachePath(appCachePath);
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setAppCacheEnabled(true);

		// 设置JS监听
		webView.addJavascriptInterface(new WMAppInterface(this), "_android");

		webView.loadUrl(this.getResources().getString(R.string.url_wm_shake));

		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);// 使用当前WebView处理跳转
				return true;// true表示此事件在此处被处理，不需要再广播
			}

			@Override
			// 转向错误时的处理
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				System.out.println(errorCode);
				System.out.println(description);
				System.out.println(failingUrl);
				Toast.makeText(WMShakeActivity.this, "Sorry，出错了！",
						Toast.LENGTH_SHORT).show();
			}
		});
		// 初始化定位
		// initLocation(this);

		// 创建监听
		player = new MediaPlayer();
		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		shake = new ShakeListener(this, player, vibrator);
	}

	// 获取location对象
	private void initLocation(Context mContext) {
		// 获得系统及服务的 LocationManager 对象 这个代码就这么写 不用考虑
		lm = (LocationManager) mContext
				.getSystemService(Context.LOCATION_SERVICE);

		// 首先检测 通过network 能否获得location对象
		// 如果获得了location对象 则更新tv
		if (startLocation(GpsProvider, mContext)) {
			updateLocation(location, mContext);
		} else if (startLocation(networkProvider, mContext)) {
			updateLocation(location, mContext);

			// 通过gps 能否获得location对象
			// 如果获得了location对象 则更新tv
		} else {
			// 如果上面两种方法都不能获得location对象 则显示下列信息
			Toast.makeText(this, "请先打开 GPS 呦亲", Toast.LENGTH_SHORT).show();
		}
	}

	protected void onResume() {
		super.onResume();

		// 获取传感器管理服务
		mSensorManager = (SensorManager) this
				.getSystemService(Service.SENSOR_SERVICE);
		// 加速度传感器
		mSensorManager.registerListener(shake,
				mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				// 还有SENSOR_DELAY_UI、SENSOR_DELAY_FASTEST、SENSOR_DELAY_GAME等，
				// 根据不同应用，需要的反应速率不同，具体根据实际情况设定
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	protected void onPause() {
		mSensorManager.unregisterListener(shake);
		super.onPause();
	}

	/**
	 * 通过参数 获取Location对象 如果Location对象为空 则返回 true 并且赋值给全局变量 location 如果为空 返回false
	 * 不赋值给全局变量location
	 * 
	 * @param provider
	 * @param mContext
	 * @return
	 */
	private boolean startLocation(String provider, final Context mContext) {
		Location location = lm.getLastKnownLocation(provider);
		// 位置监听器
		locationListener = new LocationListener() {
			// 当位置改变时触发
			@Override
			public void onLocationChanged(Location location) {
				System.out.println(location.toString());
				updateLocation(location, mContext);
			}

			// Provider失效时触发
			@Override
			public void onProviderDisabled(String arg0) {
				System.out.println(arg0);
			}

			// Provider可用时触发
			@Override
			public void onProviderEnabled(String arg0) {
				System.out.println(arg0);
			}

			// Provider状态改变时触发
			@Override
			public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
				System.out.println("onStatusChanged");
			}
		};

		// 500毫秒更新一次，忽略位置变化
		lm.requestLocationUpdates(provider, 500, 0, locationListener);

		// 如果Location对象为空 则返回 true 并且赋值给全局变量 location
		// 如果为空 返回false 不赋值给全局变量location
		if (location != null) {
			this.location = location;
			return true;
		}
		return false;

	}

	// 更新位置信息 展示到tv中
	private void updateLocation(Location location, Context mContext) {
		if (location != null) {
			System.out.println("定位对象信息如下：" + location.toString() + "/n/t其中经度："
					+ location.getLongitude() + "/n/t其中纬度："
					+ location.getLatitude());
			Toast.makeText(
					this,
					"已成功通过网络定位：( " + location.getLongitude() + ","
							+ location.getLatitude() + " )", Toast.LENGTH_LONG)
					.show();
			// 如果已经获取到location信息 则在这里注销location的监听
			// gps会在一定时间内自动关闭
			lm.removeUpdates(locationListener);
		} else {
			System.out.println("没有获取到定位对象Location");
		}
	}

	/*
	 * (non-Javadoc)退出
	 * 
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			System.exit(0);
		}
		return false;
	}
}
