package cn.jsonp.mywebview.activity;

import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import cn.jsonp.mywebview.service.ArticleService;

public class MyApplication extends Application {

	private List<Activity> activitys = new LinkedList<Activity>();

	/**
	 * 
	 */
	private Socket socket = null;

	public MyApplication() {
		System.out.println("MyApplication create");
	}

	public void startService() {
		// 启动Service
		Intent intent = new Intent();
		intent.setClass(MyApplication.this, ArticleService.class);
		MyApplication.this.startService(intent);
	}

	// 添加Activity到容器中
	public void addActivity(Activity activity) {
		if (activitys != null && activitys.size() > 0) {
			if (!activitys.contains(activity)) {
				activitys.add(activity);
			}
		} else {
			activitys.add(activity);
		}

	}

	// 遍历所有Activity并finish
	public void distroyAllActivity() {
		if (activitys != null && activitys.size() > 0) {
			for (Activity activity : activitys) {
				activity.finish();
			}
		}
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

}