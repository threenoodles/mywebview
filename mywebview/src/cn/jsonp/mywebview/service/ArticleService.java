package cn.jsonp.mywebview.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import cn.jsonp.mywebview.R;
import cn.jsonp.mywebview.activity.MainActivity;
import cn.jsonp.mywebview.activity.MyApplication;
import cn.jsonp.mywebview.utils.JSONUtil;
import cn.jsonp.mywebview.utils.Util;

public class ArticleService extends Service {

	public static final int FLAG_CONNECTION = 1;
	public static final int FLAG_PUSH = 2;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {

		System.out.println("正在启动service");
		// 开流
		new Thread() {
			@Override
			public void run() {
				super.run();
				connectServerWithTCPSocket();

			}
		}.start();
	};

	public void connectServerWithTCPSocket() {
		Socket socket = null;
		BufferedWriter writer = null;
		OutputStream out = null;
		InputStream in = null;
		BufferedReader reader = null;
		MyApplication appContext = (MyApplication) this.getApplication();
		try {// 创建一个Socket对象，并指定服务端的IP及端口号
			socket = new Socket(ArticleService.this.getResources().getString(
					R.string.url_project_ip), Integer.valueOf(this
					.getResources().getString(R.string.url_project_push_port)));
			//保存socket
			appContext.setSocket(socket);
			// 获取Socket的OutputStream对象用于发送数据。
			out = socket.getOutputStream();
			String socketData = "{uid:" + new Random().nextInt()
					+ ",action:1}\n";
			writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
			writer.write(socketData);
			writer.flush();

			while (true) {
				in = socket.getInputStream();
				reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
				String inputStr = reader.readLine();
				JSONObject result = JSONUtil.parseJSONObject(inputStr);
				// push 到来
				if (result != null
						&& result.getBoolean("success")
						&& (result.getInt("action") == ArticleService.FLAG_PUSH)) {
					String articleStr = result.getString("content");
					JSONArray articles = JSONUtil.parseJSONArray(articleStr,
							true);
					if (articles != null && articles.length() > 0) {
						JSONObject article = articles.getJSONObject(0);
						Intent startIntent = new Intent(ArticleService.this,
								MainActivity.class);
						startIntent.putExtra("aid", article.getString("id"));
						startIntent.putExtra("title",
								article.getString("title"));
						startIntent.putExtra("summary",
								article.getString("summary"));
						startIntent.putExtra("time", article.getString("time"));

						// 这里是为了判断是否是点击notification后到的详细界面
						startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						Util.showNotification(ArticleService.this,
								article.getString("title"),
								article.getString("summary"), 98, startIntent);

					}
					// 初始化连接
				} else if (result != null
						&& result.getBoolean("success")
						&& (result.getInt("action") == ArticleService.FLAG_CONNECTION)) {

					// Intent startIntent = new Intent(ArticleService.this,
					// MainActivity.class);
					// // 这里是为了判断是否是点击notification后到的详细界面
					// startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					// Util.showNotification(ArticleService.this, "【JSONP】",
					// "初始化连接成功", 99,startIntent);

				} else {
					System.out.println(result == null ? result : result
							.toString());
				}
				System.out.println("sleeping...");

				Thread.sleep(5000l);
			}
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (out != null) {
					out.close();
				}
				if (writer != null) {
					writer.close();
				}
				if (reader != null) {
					reader.close();
				}

				if (socket != null) {
					socket.close();
				}
			} catch (Exception e) {

			}
		}
	}

	@Override
	public void onStart(Intent intent, int startId) {

	};

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}
