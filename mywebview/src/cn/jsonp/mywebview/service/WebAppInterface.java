package cn.jsonp.mywebview.service;

import java.util.Map;

import org.json.JSONArray;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;
import cn.jsonp.mywebview.utils.DatabaseHelper;
import cn.jsonp.mywebview.utils.NetUtil;

public class WebAppInterface {

	Context mContext;

	/** Instantiate the interface and set the context */
	public WebAppInterface(Context c) {
		mContext = c;
	}

	/** Show a toast from the web page */
	@JavascriptInterface
	public void showToast(String toast) {
		Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
	}

	@JavascriptInterface
	public int network() {

		return NetUtil.checkInternet(mContext);
	}

	@JavascriptInterface
	public String getArticle(String aid) {

		try {
			return DatabaseHelper.queryForJSONObject(mContext,
					"select id,detail from j_article_detail where id = ?",
					new String[] { String.valueOf(aid) }, new String[] { "id",
							"detail" });
		} catch (Exception e) {
			return "";
		}
	}
	
	@JavascriptInterface
	public String getArticleIds() {
		
		try {
			JSONArray articles = DatabaseHelper.queryForArr(mContext, "j_article_detail", new String[] { "id"});
			
			JSONArray results = new JSONArray();
			for(int i= 0,n = articles.length();i<n;i++){
				results.put(articles.getJSONObject(i).getString("id"));
			}
			return results.toString();
		} catch (Exception e) {
			return "";
		}
	}

	@JavascriptInterface
	public void updateArticle(String aid, String detail) {
		Map<String, String> article = DatabaseHelper.query(mContext,
				"select id,detail from j_article_detail where id = ?",
				new String[] { String.valueOf(aid) }, new String[] { "id",
						"detail" });
		if (article.size() > 0) {
			DatabaseHelper.update(mContext,
					"update j_article_detail set detail = ? where id = ?",
					new String[] { detail, aid });
		} else {
			DatabaseHelper.update(mContext,
					"insert into j_article_detail values(?,?)", new String[] {
							aid, detail });
		}
	}
}
