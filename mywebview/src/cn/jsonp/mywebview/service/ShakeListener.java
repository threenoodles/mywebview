package cn.jsonp.mywebview.service;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.media.MediaPlayer;
import android.os.Vibrator;
import cn.jsonp.mywebview.R;
import cn.jsonp.mywebview.utils.Util;

public class ShakeListener implements SensorEventListener {

	// 是否可以播放音乐
	// private static boolean FLAG_PLAY = true;

	// 监听器
	private Timer timer = new Timer();
	private Activity context;
	private MediaPlayer player;
	private Vibrator vibrator;
	// 请求新菜单的状态
	public static int FLAG_LOAD = 1;
	public static final int FLAG_LOADING = 0;
	public static final int FLAG_LOADED = 1;
	// 是否正在摇动 1 是 0 否
	public static int FLAG_SHAKE = 0;
	public static final int FLAG_SHAKING = 1;
	public static final int FLAG_SHAKED = 0;

	public ShakeListener(Activity context, MediaPlayer player, Vibrator vibrator) {
		super();
		this.context = context;
		this.player = player;
		this.vibrator = vibrator;
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		int sensorType = event.sensor.getType();
		// values[0]:X轴，values[1]：Y轴，values[2]：Z轴
		float[] values = event.values;

		if (sensorType == Sensor.TYPE_ACCELEROMETER) {
			/*
			 * 正常情况下，任意轴数值最大就在9.8~10之间，只有在突然摇动手机 的时候，瞬时加速度才会突然增大或减少。
			 * 监听任一轴的加速度大于17即可
			 */
			if ((Math.abs(values[0]) > 19 || Math.abs(values[1]) > 19 || Math
					.abs(values[2]) > 19)) {
				// context.overridePendingTransition(R.anim.zoom_out_enter,
				// R.anim.zoom_out_exit);
				// 检测到晃动后启动OpenDoor效果
				System.out.println("检测到晃动！！！");
				System.out.println("ShakeListener : "
						+ ShakeListener.FLAG_SHAKE);
				// 播放音乐
				this.playShaking();

				// 振动
				Util.PlayVibrator(vibrator);

			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// 当传感器精度改变时回调该方法，Do nothing.
	}

	// 播放成功的音乐
	public void playSuccess() {

		// this.play(context.getResources().getString(R.string.url_wm_shake_success));

		// 播放铃声
		try {
			// 设置正在摇动
			FLAG_SHAKE = FLAG_SHAKING;
			player = new MediaPlayer();
			player.setDataSource(context.getResources().getString(R.string.url_wm_shake_success));
			Util.playRing(player, context);
			TimerTask task = null;
			task = new TimerTask() {
				@Override
				public void run() {
					try {
						// 初始化摇动的参数
						FLAG_SHAKE = FLAG_SHAKED;
						player.stop();
						player = new MediaPlayer();
					} catch (Exception e) {
						FLAG_SHAKE = FLAG_SHAKED;
						player = new MediaPlayer();
					}
				}
			};
			timer.schedule(task, 3000);
		} catch (Exception e) {

		}
	}

	// 播放摇动中的音乐
	public void playShaking() {
		this.play(context.getResources().getString(R.string.url_wm_shakeing));
	}

	// 播放的音乐
	public void play(String url) {
		if (FLAG_SHAKE == FLAG_SHAKED) {
			// 播放铃声
			try {
				// 设置正在摇动
				FLAG_SHAKE = FLAG_SHAKING;
				player.setDataSource(url);
				Util.playRing(player, context);
				TimerTask task = null;
				task = new TimerTask() {
					@Override
					public void run() {
						try {
							// 初始化摇动的参数
							FLAG_SHAKE = FLAG_SHAKED;
							player.stop();
							player = new MediaPlayer();
						} catch (Exception e) {
							FLAG_SHAKE = FLAG_SHAKED;
							player = new MediaPlayer();
						}
					}
				};
				timer.schedule(task, 3000);
			} catch (Exception e) {

			}
		}
	}

}