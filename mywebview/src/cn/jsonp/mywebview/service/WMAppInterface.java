package cn.jsonp.mywebview.service;

import android.content.Context;
import android.content.Intent;
import android.webkit.JavascriptInterface;
import android.widget.Toast;
import cn.jsonp.mywebview.activity.WMShakeActivity;
import cn.jsonp.mywebview.utils.NetUtil;

public class WMAppInterface {

	Context mContext;

	/** Instantiate the interface and set the context */
	public WMAppInterface(Context c) {
		mContext = c;
	}

	/** Show a toast from the web page */
	@JavascriptInterface
	public void showToast(String toast) {
		Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
	}

	@JavascriptInterface
	public int network() {
		return NetUtil.checkInternet(mContext);
	}
	
	@JavascriptInterface
	public void startShake() { 
		Intent intent = new Intent();
		intent.setClass(mContext, WMShakeActivity.class);
		mContext.startActivity(intent);
	}
	
	@JavascriptInterface
	public int getShakeStatus() { 
		
		return ShakeListener.FLAG_SHAKE;
	}
	
	@JavascriptInterface
	public void setAjaxStatus(int st) { 
		
		ShakeListener.FLAG_LOAD = st;
	}
	
	@JavascriptInterface
	public void loadSuccess() { 
		System.out.println("===============>loadSuccess");
		WMShakeActivity.shake.playSuccess();
	}
}
